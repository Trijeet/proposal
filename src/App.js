import 'bootstrap/dist/css/bootstrap.css';
import './App.css';
import ActiveBox from './components/active-box/active-box';
import ToggleMenu from './components/toggle-menu/toggle-menu';
import React, { useState } from 'react';

import Cookies from 'universal-cookie';

const cookies = new Cookies();

function App() {

  const [start, setStart] = useState(false);
  const [active_puzzle, setActivePuzzle] = useState("start");

  const [solved_puzzles, setSolvedPuzzles] = useState({
      past: false,
      present: false,
      future: false,
      final_puzzle: false
  });

  return (
    <div className="global-styles">
      <ToggleMenu setActivePuzzle={setActivePuzzle} />
      <ActiveBox active_puzzle={active_puzzle} start={start} setStart={setStart} setActivePuzzle={setActivePuzzle} solved_puzzles={solved_puzzles} setSolvedPuzzles={setSolvedPuzzles} cookies={cookies} />
    </div>
  );
}

export default App;
