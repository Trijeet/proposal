import React, { useState, useEffect } from 'react'
import MenuItem from '../menu-item/menu-item';
import Quiz from '../quiz/quiz';
import Start from '../start/start';
import ImageCarousel from '../pictured/pictured';
import Vault from '../vault/vault';
import Wordle from '../wordle-clone/wordle-clone';
import './active-box.styles.css';

const ActiveBox = ({ active_puzzle, setStart, setActivePuzzle, solved_puzzles, setSolvedPuzzles, cookies }) => {

    const [images, setImages] = useState();
    

    useEffect(() => {
        const my_images = [
            "https://i.imgur.com/HZVZt5M.jpg",
            "https://i.imgur.com/FebWZiJ.jpg",
            "https://i.imgur.com/HkCx0vR.jpg",
            "https://i.imgur.com/PJlx7iA.jpg",
            "https://i.imgur.com/xPgumh5.jpg",
            "https://i.imgur.com/EYhfnXA.jpg",
            "https://i.imgur.com/mbpPQzD.jpg",
            "https://i.imgur.com/WFp978F.jpg",
            "https://i.imgur.com/oIplOqk.jpg",
            "https://i.imgur.com/7yd605j.jpg",
            "https://i.imgur.com/FElLa7r.jpg"


        ]

        const my_hints = [
            "The second letter of the animal we came to watch here",
            "The first letter of the animal that we came to see here",
            "The first letter of the month this was taken",
            "The first letter of the state this was taken in",
            "The first letter of the event where this photo was taken",
            "The the most common letter in the highlight event of this trip",
            "The first letter of the city we were going to here",
            "The second letter of the type of coffee you introduced me to here",
            "The first letter of the family who built the property we visited here",
            "The second letter of the country we visited here",
            "The first letter of the person who went with us on this trip"
        ]

        setImages(
          Array.from(Array(11).keys()).map((id) => ({
            id,
            url: my_images[id],
            hint: my_hints[id] || "Hello"
          }))
        );
      }, []);

    let display_component = null;

    switch(active_puzzle) {
        case "past":
            display_component = <ImageCarousel images={images} />;
            break
        case "present":
            display_component = <Quiz setActivePuzzle={setActivePuzzle} />;
            break
        case "future":
            display_component = <Wordle cookies={cookies} />;
            break
        case "final_puzzle":
            display_component = MenuItem;
            break
        case "start":
            display_component = <Start setStart={ setStart } solved_puzzles={solved_puzzles} setActivePuzzle={setActivePuzzle} />;
            break
        case "vault":
            display_component = <Vault solved_puzzles={solved_puzzles} setSolvedPuzzles={setSolvedPuzzles}/>
            break
        default:
            display_component = MenuItem;
            break
    }
 
    return (
        <div className="active-state">
            <h1 className='headliner'>T's Funhouse of Love</h1>
            {display_component}
        </div>
    )
}

export default ActiveBox