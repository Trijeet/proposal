import React from 'react'
import './code-entry.styles.css';


const CodeEntry = ({ name, value, handleChange }) => {
 
    return (
        <input
            name={name}
            className="code-cell"
            type="text"
            value={ value }
            onChange={ handleChange }
        />
    )
}

export default CodeEntry;