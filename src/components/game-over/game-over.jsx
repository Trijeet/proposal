import './game-over.styles.css';


const GameOver = ( { pts, setActivePuzzle } ) => {

    const goHome = () => {
        setActivePuzzle("start");
    }

    return (
        <>
            <div className='title'>Quiz Complete</div>

            <div className='points'>You got {pts} out of 25 correct</div>
            {
                (pts>22) ? <div className='points'>The code is our two-digit birthday months, first mine then yours.</div> : <div className='points'>Talk to the man and try again! </div>
            }

            <button className="main-button" onClick={goHome}>Return Home</button>
        </>
    )
}

export default GameOver