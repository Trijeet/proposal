import React from 'react'
import './menu-item.styles.css';


const MenuItem = ({ menuAction, menu_text }) => {
 
    return (
        <button className="menu-button" onClick={menuAction}>{menu_text}</button>
    )
}

export default MenuItem