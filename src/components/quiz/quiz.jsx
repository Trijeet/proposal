import React, { useState, useEffect } from 'react'
import GameOver from '../game-over/game-over';
import './quiz.styles.css';
import quiz_questions from './quiz_questions';

const Quiz = ( { setActivePuzzle } ) => {

    const [quiz, setQuiz] = useState([]);
    const [number, setNumber] = useState(0);
    const [pts, setPts] = useState(0);

    const shuffle = (arr) => arr.sort(() => Math.random() - 0.5);

    const pickAnswer = (e) => {

        let userAnswer = e.target.outerText;

        if (quiz[number].answer === userAnswer) setPts(pts + 1);
        setNumber(number + 1);
    }

    useEffect(() => {

        setQuiz(quiz_questions.results.map(item => (

            {
                question: item.question,
                options: shuffle([...item.incorrect_answers, item.correct_answer]),
                answer: item.correct_answer
            }

        )));

    }, []);


    return (
        <div className='window'>
            { quiz[number] &&

                <>
                    {number===0 ? <h2 className="challenge-text">Challenge: get at least 22 correct on this quiz</h2>: <p></p>}
                    <div className='question' dangerouslySetInnerHTML={{ __html: quiz[number].question }}></div>

                    <div className='option-container'>
                        {quiz[number].options.map((item, index) => (
                            <div className='options' key={index} dangerouslySetInnerHTML={{ __html: item }} onClick={pickAnswer}></div>
                        ))}
                    </div>

                    <div className='question' dangerouslySetInnerHTML={{ __html: number+ " of 25" }}></div>
                </>

            }
            {
                number === 26 && <GameOver pts={pts} setActivePuzzle={setActivePuzzle} />
            }
        </div>
    )
}

export default Quiz