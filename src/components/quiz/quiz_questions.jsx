const quiz_questions = {
    "response_code":0,
    "results": [
        {
            "category": "Love",
            "type":"multiple",
            "difficulty":"easy",
            "question": "When in doubt, ask Oprah. Here are her 25 questions to ask before marriage. Can you guess my answers to each?",
            "correct_answer":"Yes",
            "incorrect_answers":["I Guess","The Questions Come Anyways, Don't They?","HELP!"]
        },
        {
            "category": "Love",
            "type":"multiple",
            "difficulty":"easy",
            "question": "Why marriage? What will marriage add to the relationship?",
            "correct_answer":"Showing the world what we already know- that we're together forever",
            "incorrect_answers":["Legal use of 'my wife' in conversation","Having the same last name","Sharing the same financial assets"]
        },
        {
            "category": "Love",
            "type":"multiple",
            "difficulty":"easy",
            "question": "How do you handle change and unexpected?",
            "correct_answer":"Change is just life. Anything is fine as long as I'm with you.",
            "incorrect_answers":["Panic!","Ask you to make banana bread","Go take a nap"]
        },
        {
            "category": "Love",
            "type":"multiple",
            "difficulty":"easy",
            "question": "How well do we currently handle disagreements with each other?",
            "correct_answer":"I can hardly count on one hand the number of times we fight. Usually solved by sleeping.",
            "incorrect_answers":["0/10","2/10","1/10"]
        },
        {
            "category": "Love",
            "type":"multiple",
            "difficulty":"easy",
            "question": "How much do you value time together, versus time apart?",
            "correct_answer":"I think we are squarely in the time together is always better camp",
            "incorrect_answers":["We're like a good half-and-half","Distance makes the heart fonder","Time apart makes one smart"]
        },
        {
            "category": "Love",
            "type":"multiple",
            "difficulty":"easy",
            "question": "Is your parent's marriage part of your inspiration to marry?",
            "correct_answer":"Mostly the opposite. But you can learn from non-examples",
            "incorrect_answers":["They are my perfect couple ideal","I don't have positive or negative feelings","On some days they are inspirational"]
        },
        {
            "category": "Love",
            "type":"multiple",
            "difficulty":"easy",
            "question": "Do you want kids?",
            "correct_answer":"As many as we can reasonably have!",
            "incorrect_answers":["No kids for me","The standard two is best","Let's decide when we get there"]
        },
        {
            "category": "Love",
            "type":"multiple",
            "difficulty":"easy",
            "question": "What if we're not able to have biological children?",
            "correct_answer":"Then we can explore alternative options, but kids are a priority",
            "incorrect_answers":["Then we adopt","No kids at that point","Get thirty cats"]
        },
        {
            "category": "Love",
            "type":"multiple",
            "difficulty":"easy",
            "question": "How do you see kids fitting into our life?",
            "correct_answer":"Once we have kids, they are the biggest priority",
            "incorrect_answers":["Second only to baking cookies","Life as normal, but with some extra small ones","All our free hours for taking care of the kids"]
        },
        {
            "category": "Love",
            "type":"multiple",
            "difficulty":"easy",
            "question": "How will you handle it if we drift apart?",
            "correct_answer":"Remind you of our love in two forms: take more trips abroad and to the bed! We'll work to make sure that doesn't happen",
            "incorrect_answers":["Dazzle you with my hidden talent of geometric art","Remind you of the time we first fell in love by taking you to Taco Son","Tie us firmly together, duh"]
        },
        {
            "category": "Love",
            "type":"multiple",
            "difficulty":"easy",
            "question": "How do you expect to cope with our sexual ebbs and flows?",
            "correct_answer":"Good communication is the key to any sexual success",
            "incorrect_answers":["Rub our butts together to reorient the flow","Flow smoothly like the tide","Reignite passion with some Dune spice"]
        },
        {
            "category": "Love",
            "type":"multiple",
            "difficulty":"easy",
            "question": "How do you expect to get sexual needs met, if I’m not meeting them?",
            "correct_answer":"Let's talk about what's causing the supply chain shortage",
            "incorrect_answers":["Strip club, straight away","Sing sad cowboy songs to let you know","Start a riot"]
        },
        {
            "category": "Love",
            "type":"multiple",
            "difficulty":"easy",
            "question": "How do you imagine spending the holidays?",
            "correct_answer":"With you and with family- having everyone around our family if we can",
            "incorrect_answers":["Eating lots of sweets and ice cream","Christmas carols on every holiday","Travelling to Tampa"]
        },
        {
            "category": "Love",
            "type":"multiple",
            "difficulty":"easy",
            "question": "What’s your take on vacations, and how often would you like to take them?",
            "correct_answer":"Everyday should be a vacation, but big international trips: once per year; lots of weekend trips, though!",
            "incorrect_answers":["Vacations should be once every three years","Vacations are just road blocks to a successful career","Once a month"]
        },
        {
            "category": "Love",
            "type":"multiple",
            "difficulty":"easy",
            "question": "Do you want to save a lot early on, or save up in bursts for things like a vacation or a new TV?",
            "correct_answer":"Savings? What are those? We have debt, mortgage, and a nest egg to think about.",
            "incorrect_answers":["Save with retirement in mind","Save up in short bursts for that big vacation","Save for our children"]
        },
        {
            "category": "Love",
            "type":"multiple",
            "difficulty":"easy",
            "question": "Do you want separate bank accounts, or to share all assets?",
            "correct_answer":"All assets are shared. One bank account, one dream",
            "incorrect_answers":["Same bank account but separate investments","Different bank accounts","Same assets and investments but different bank accounts"]
        },
        {
            "category": "Love",
            "type":"multiple",
            "difficulty":"easy",
            "question": "Do we agree on the division of labor in our house?",
            "correct_answer":"Everything has to get done- you see it, you do it. If it's big, let's plan together. Division of labor is a dumb idea.",
            "incorrect_answers":["I do dishes, you do laundry, we both sleep","I do the cooking, you do the cleaning. You do the baking, I do the cleaning.","I rub your neck- you rub my neck"]
        },
        {
            "category": "Love",
            "type":"multiple",
            "difficulty":"easy",
            "question": "When do you feel the most loved by me?",
            "correct_answer":"Laying in bed getting my head scratched with banana bread in the oven",
            "incorrect_answers":["When you buy me a nice gift","When you surprise me with a nice vacation idea","When I come home and things I was going to do are already done"]
        },
        {
            "category": "Love",
            "type":"multiple",
            "difficulty":"easy",
            "question": "How do you express love?",
            "correct_answer":"Spend time with you- let's do things you find fun, eat things you like, talk about things you care about.",
            "incorrect_answers":["Take you to eat pounds and pounds of crab","Buy you nice jewelry and accessories","Kiss and nuzzle your cheeks repeatedly"]
        },
        {
            "category": "Love",
            "type":"multiple",
            "difficulty":"easy",
            "question": "When you reflect back on your childhood, what memories bring the most joy? Which bring the most pain?",
            "correct_answer":"I don't feel any particular way about my childhood.",
            "incorrect_answers":["Happiest when I first laid eyes on you, saddest whenever you left me","Happiest when I dropped out, most pain when I broke my knee","How can I fit this into one line? So much good, so much bad."]
        },
        {
            "category": "Love",
            "type":"multiple",
            "difficulty":"easy",
            "question": "Do we appropriately respect any religious, spiritual, or political differences between us?",
            "correct_answer":"We should talk about any differences we have. I think it's important to be on the same/compatible pages- and reconcile our differences",
            "incorrect_answers":["Of course, these things are just belief and can change at any time","Do we have any differences between us?","We both can hold whatever beliefs we feel are important"]
        },
        {
            "category": "Love",
            "type":"multiple",
            "difficulty":"easy",
            "question": "Whose career would take precedence, if it became necessary?",
            "correct_answer":"We would make the best decision for our family and children.",
            "incorrect_answers":["The one with a higher salary","Definitely mine","Definitely yours"]
        },
        {
            "category": "Love",
            "type":"multiple",
            "difficulty":"easy",
            "question": "Do either of us have any major secrets we haven't yet shared?",
            "correct_answer":"I don't think so... what counts as major?",
            "incorrect_answers":["Secrets are overrated","I think it's probably better if there is a secret or two","Got anything to share?"]
        },
        {
            "category": "Love",
            "type":"multiple",
            "difficulty":"easy",
            "question": "Are you committed to counseling, if and when we need it?",
            "correct_answer":"No. We should solve our problems ourselves, we are a team- even if we're fighting.",
            "incorrect_answers":["We'll cross that bridge when we get there","I would rather talk through things with our close family","Of course, everybody needs counseling sometimes"]
        },
        {
            "category": "Love",
            "type":"multiple",
            "difficulty":"easy",
            "question": "What're you looking forward to the most?",
            "correct_answer":"Exploring a whole new life with you and adding to what the world has to offer",
            "incorrect_answers":["Being able to reference you as my wife instead of my girl","Kids are just around the corner","Sharing the same last name"]
        },
        {
            "category": "Love",
            "type":"multiple",
            "difficulty":"easy",
            "question": "What's a promise you can make now and keep forever?",
            "correct_answer":"That I will always love and support you until the end of time",
            "incorrect_answers":["That I will terrorize your belly button for 12 hours at a time","We will keep the same sleep schedule until the end of time","We will only have 1 child"]
        }
    ]
}

export default quiz_questions