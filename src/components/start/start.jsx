import React from 'react'
import './start.styles.css';


const Start = ({ setStart, solved_puzzles, setActivePuzzle }) => {

    const beginPast = () => {
        setStart(true);
        setActivePuzzle("past");
    }

    const beginPresent = () => {
        setStart(true);
        setActivePuzzle("present");
    }

    const beginFuture = () => {
        setStart(true);
        setActivePuzzle("future");
    }
 
    return (
        <div className="intro">
            <h1>Pick a Puzzle</h1>
            <div>
                {solved_puzzles["past"] ? <button className="solved-button">Solved</button> : <button className="main-button" onClick={beginPast}>Past</button> }
                {solved_puzzles["present"] ? <button className="solved-button">Solved</button> : <button className="main-button" onClick={beginPresent}>Present</button>}
                {solved_puzzles["future"] ? <button className="solved-button">Solved</button> : <button className="main-button" onClick={beginFuture}>Future</button>}
            </div>
            
        </div>
    )
}

export default Start