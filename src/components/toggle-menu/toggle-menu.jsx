import React, { useState } from 'react'
import MenuItem from '../menu-item/menu-item';
import './toggle-menu.styles.css';


const ToggleMenu = ({ setActivePuzzle }) => {

    const [visible, setVisible] = useState(false);

    const toggleMenu = () => {
        setVisible(!visible);
    }

    const goHome = () => {
        setActivePuzzle("start");
    }

    const beginPresent = () => {
        setActivePuzzle("vault");
    }

    // const beginFuture = () => {
    //     setActivePuzzle("start");
    // }
 
    return (
        <div>
            <button className="toggle-button" onClick={toggleMenu}>Show Menu</button>
            {visible && <div alignment="right">
            <MenuItem hash="first-page" menuAction={goHome} menu_text={"Home"} />
            <MenuItem hash="second-page" menuAction={beginPresent} menu_text={"Add Key"} />
            {/* <MenuItem hash="third-page" menuAction={beginFuture} menu_text={"View Progress"} /> */}
            </div>}
        </div>
    )
}

export default ToggleMenu