import React, { useState } from 'react'
import CodeEntry from '../code-entry/code-entry';

import './vault.styles.css';


const Vault = ( { solved_puzzles, setSolvedPuzzles } ) => {

    const [form_codes, setFormCodes] = useState({ 
        past: "", present: "", future: "",
        past_success: false, present_success: false, future_success: false
    });

    const codeOnInput = (e) => {
        const key = e.target.name;
        const value = e.target.value;

        setFormCodes({ ...form_codes, [key]: value });
    };

    const correct_ones = {
        past: "ilovetravel",
        present: "0307",
        future: "paris"
    };

    let incorrect_ones = {
        past: "",
        present: "",
        future: ""
    };

    let update_markers = {
        past: false,
        present: false,
        future: false
    };

    let update_markers_success = {
        past_success: false,
        present_success: false,
        future_success: false
    };

    const handleSubmit = (e) => {
        e.preventDefault();

        const loopers = ["past", "present", "future"];
        for (let i=0; i<3; i++) {
            if (form_codes[loopers[i]].toLowerCase()) {
                console.log("%s Value Entered" % loopers[i]);
                if (form_codes[loopers[i]].toLowerCase()===correct_ones[[loopers[i]]]) {
                    update_markers[loopers[i]] = true;
                    update_markers_success[loopers[i]+"_success"] = true;

                } else {
                    incorrect_ones[loopers[i]] = "This code is not correct";
                }
            }
        }

        setFormCodes({ ...form_codes, ...update_markers_success});
        setSolvedPuzzles({...solved_puzzles, ...update_markers})
    }

    return (
        <div className="vault-box">
            <form className='vault-form' onSubmit={handleSubmit}>
            { ( form_codes.past_success && form_codes.present_success && form_codes.future_success ) ? 
                <div className='hidden-msg-container'>
                    <div className='hidden-msg-title'>
                        To the great love of my life,
                    </div>
                    <div className='hidden-msg-body'>
                        This little project came to be for two reasons: one, I wanted us both to have some fun; two, I wanted to immortalize and remind you of all of the wonderful things that we have done - and more importantly, will do - in the future. Each part of this puzzle project represents an important element to the oath that I will make to you. The past, where we have had so much fun, explored so much, and started building the foundation for the most exciting life I can imagine. The present, to show you exactly who I am and to promise here that I will change- because of you, for the better. The future, to say that the good times will only continue and maybe even get more grand. By now, you should know that we're going to Paris for our trip. It's a tad cliche, but I could think of no better place to spend a week enraptured in ourselves than the city of love. While we take in the marvels of one of the most miraculous cities on Earth, I've prepared a series of oaths I want to make to you as we prepare to spend the rest of our lives together. I'll make the first one here: I swear that I will stand by you forever and fill each of your days with wonder and excitement. May we never tire of this beautiful place we live, and let's dedicate our lives to enjoying it even more through each other. I love you.
                    </div>
                    <div className='hidden-msg-title'>
                        --Trijeet
                    </div>
                </div>
                
                :
                <div>
                    {incorrect_ones["past"] && !form_codes["past_success"] ?  <h2 className="code-header">Your past code is incorrect</h2> : <span></span>}
                    <h2 className="code-feedback">Check Your Codes</h2>

                    <span className="code-block">
                        <label className="code-cell" htmlFor={"past"}> Past Code: </label> 
                        { form_codes.past_success ? <span className="code-success" > Success </span> : <CodeEntry name="past" value={form_codes.past} handleChange={codeOnInput}/> }
                    </span>

                    <span className="code-block">
                        <label className="code-cell" htmlFor={"present"}> Present Code: </label>
                        { form_codes.present_success ? <span className="code-success" > Success </span> : <CodeEntry name="present" value={form_codes.present} handleChange={codeOnInput}/> }
                    </span>

                    <span className="code-block">
                        <label className="code-cell" htmlFor={"future"}> Future Code: </label>
                        { form_codes.future_success ? <span className="code-success" > Success </span> : <CodeEntry name="future" value={form_codes.future} handleChange={codeOnInput}/> }
                    </span>

                    <input className="code-enter" type="submit" value="Check Codes" />
                </div>
            }
            </form>
        </div>
    )
}

export default Vault;